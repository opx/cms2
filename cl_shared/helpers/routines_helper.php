<?php

if (!function_exists('setValidation')) {
    function setValidation($status, $msg = null, $tipe = '', $section = '') {
        $data = [];
        if (!empty($msg)) {
            foreach (array_keys($msg) as $item) {
                $data[$item] = $msg[$item];
            }
        }

        $output = [
            'status'=>$status,
            'tipe'=>$tipe,
            'section'=>$section,
            'msg'=> $data
        ];
        return $output;
    }
}

# cek empty var, kecuali numerik di-bypass
if (!function_exists('isEmptyVar')) {
    function isEmptyVar(&$var) {
        $retval = true;
        if (isset($var)) {
            if (!is_numeric($var)) {
                if (empty($var)) {
                    $retval = true;
                } else {
                    $retval = false;
                }
            } else {
                $retval = false;
            }
        } else {
            $retval = true;
        }
        return $retval;
    }
}

if (!function_exists('retAs')) {
    function retAs($val, $as = VT_OBJECT) {
        switch ($as) {
            case VT_OBJECT:
                return (object)$val;
            case VT_INTEGER:
                return (int)$val;
            case VT_STRING:
                return (string)$val;
            case VT_FLOAT:
                return (float)$val;
            case VT_BOOL:
                return (bool)$val;
        }
    }
}

if (!function_exists('validateEmptyParams')) {
    function validateEmptyParams($keys, $params) {
        $process = setProcessMsg(true);

        if (empty($keys)) {
            return $process;
        }

        if (empty($params)) {
            $process = setProcessMsg(false, 'Invalid Parameters');
        }

        if ($process['status']) {
            foreach ($keys as $key => $msg) {
                if (isset($params[$key])) {
                    if (!is_numeric($params[$key])) {
                        if (empty($params[$key])) {
                             $process = setProcessMsg(false, $msg);
                             break;
                        }
                    }
                } else {
                    $process = setProcessMsg(false, $msg);
                    break;
                }
            }
        }
        return $process;
    }
}

if (!function_exists('validateEmptyParams2')) {
    function validateEmptyParams2($keys, $params) {
        $process = setValidation(true);

        if (empty($keys)) {
            return $process;
        }

        if (empty($params)) {
            $process = setValidation(false, [0 => 'Invalid Parameters']);
        }

        if ($process['status']) {
            foreach ($keys as $key => $msg) {
                if (isset($params[$key])) {
                    if (!is_numeric($params[$key])) {
                        if (empty($params[$key])) {
                             $process = setValidation(false, [$key => $msg]);
                             break;
                        }
                    }
                } else {
                    $process = setValidation(false, [$key => $msg]);
                    break;
                }
            }
        }
        return $process;
    }
}



if (!function_exists('setProcessMsg')) {
    function setProcessMsg($status, $msg = null) {
        return [
            'status'    => $status,
            'msg'       => $msg,
        ];
    }
}

if (!function_exists('global_getImageDir')) {
    function global_getImageDir($path) {
        return DOMAIN_IMAGEDIR.$path.DIRECTORY_SEPARATOR;
    }
}

if (!function_exists('global_imageUrl')) {
    function global_imageUrl($path) {
        return DOMAIN_URL.'images/'.$path;
    }
}


if (!function_exists('setVal')) {
    function setVal($val, $default = null) {
        if (empty($val)) {
            return $default;
        } else {
            return $val;
        }
    }
}
