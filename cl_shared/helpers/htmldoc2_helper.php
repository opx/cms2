<?php

if (!function_exists('asset_url')) {
    function asset_url($path) {
        return site_url($path);
    }
}

if (!function_exists('addAssetJs')) {
    function addAssetJs($path, $options = []) {
        addJs(asset_url($path), $options);
    }
}

if (!function_exists('addAssetJs2')) {
    function addAssetJs2($path, array $files) {
        addJsFromPath(asset_url($path), $files);
    }
}

if (!function_exists('addAssetCss')) {
    function addAssetCss($path, $options = []) {
        addCss(asset_url($path), $options);
    }
}

if (!function_exists('addAssetCss2')) {
    function addAssetCss2($path, array $files) {
        addCssFromPath(asset_url($path), $files);
    }
}
