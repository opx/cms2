<?php

if (!function_exists('parseTag')) {
    function parseTag($attributes) {
        $tag = '';
        foreach ($attributes as $key => $value) {
            $tag .= sprintf(' %s="%s"',$key, $value);
        }
        return $tag;
    }
}

if (!function_exists('addJs')) {
    function addJs($path, $options = [], $type='public') {
        $fmt = '<script type="text/javascript" src="%s"%s></script>';
        $attributes = '';
        if (!empty($options)) {
            $attributes = parseTag($options);
        }
        printf($fmt, $path, $attributes);
    }
}

if (!function_exists('addCss')) {
    function addCss($path, $options = [], $type='public') {
        $fmt = '<link rel="stylesheet" href="%s"%s>';
        $attributes = '';
        if (!empty($options)) {
            $attributes = parseTag($options);
        }
        printf($fmt, $path, $attributes);
    }
}

if (!function_exists('addJsFromPath')) {
    function addJsFromPath($path, array $files) {
        $url = (lastChar($path) !== '/') ? $path.'/' : $path;
        foreach ($files as $file) {
            addJs($url.$file);
        }
    }
}

if (!function_exists('addCssFromPath')) {
    function addCssFromPath($path, array $files) {
        $url = (lastChar($path) !== '/') ? $path.'/' : $path;
        foreach ($files as $file) {
            addCss($url.$file);
        }
    }
}

if (!function_exists('addAssetJs')) {
    function addAssetJs($path, $options = []) {
        addJs(asset_url($path), $options);
    }
}

if (!function_exists('addAssetJs2')) {
    function addAssetJs2($path, array $files) {
        addJsFromPath(asset_url($path), $files);
    }
}

if (!function_exists('addAssetCss2')) {
    function addAssetCss2($path, array $files) {
        addCssFromPath(asset_url($path), $files);
    }
}

if (!function_exists('addAssetCss')) {
    function addAssetCss($path, $options = []) {
        addCss(asset_url($path), $options);
    }
}
