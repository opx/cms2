<?php

/*
    String Manipulation Helper
    @File  : strutils
    @Author: opx (mockheroicx@yahoo.com)
*/

if (!function_exists('lastChar')) {
    function lastChar($str) {
        $len = strlen($str);
        return substr($str, $len - 1);
    }
}

if (!function_exists('parseAttributes')) {
    function parseAttributes($attributes) {
        $attribute = '';
        if (count($attributes)) {
            foreach($attributes as $key => $value) {
                $attribute .= ' '.$key.'="'.$value.'"';
            }
        }
        return $attribute;
    }
}

if (!function_exists('dquoteStr')) {
    function dquoteStr($str) {
        return sprintf('"%s"', $str);
    }
}

if (!function_exists('numFmt')) {
    function numFmt($val) {
        return number_format($val, 0, ',', '.');
    }
}

if (!function_exists('currencyFmt')) {
    function currencyFmt($val, $matauang = '') {
        return $matauang.number_format($val, 0, ',', '.');
    }
}

if (!function_exists('extractDate')) {
    function extractDate($str, $wtime = false, $separator = '/') {
        $fmt = sprintf('d%sm%sY', $separator, $separator);
        if ($wtime) {
            $fmt .= ' H:i:s';
        }
        if ($str != 0) {
            return (new DateTime($str))->format($fmt);
        } else {
            return '00-00-0000';
        }
    }
}

if (!function_exists('strToInt')) {
    function strToInt($str) {
        try {
            $retval = (int)$str;
        } catch (Exception $e) {
            $retval = 0;
        }

        return $retval;
    }
}

if (!function_exists('validateUrlSlash')) {
    function validateUrlSlash($url, $append_slash = true) {
        $urls = parse_url($url);
        $scheme = $urls['scheme'];
        $scheme_len = strlen($scheme);
        # http, https, ftp +  ://
        if (!empty($scheme)) {
            $val = $urls['path'];
        } else {
            $val = $url;
        }
        if ($append_slash) {
            if (lastChar($val) !== '/') {
                $val .= '/';
            }
        }
        $val = str_replace('//', '/', $val);

        if (!empty($scheme)) {
            return $scheme.'://'.$urls['host'].$val;
        } else {
            return $val;
        }
    }
}

if (!function_exists('limitStr')) {
    function limitStr($str, $limit, $suffix = '...') {
        $newstr = substr($str, 0, $limit);
        if (strlen($str) > $limit) {
            $newstr .= $suffix;
        }
        return $newstr;
    }
}

if (!function_exists('setVal')) {
    function setVal(&$value, $default = null) {
        $retval = $default;
        if (isset($value)) {
            if (!isEmptyVar($value)) {
                $retval = $value;
            }
        } else {
            $retval = $default;
        }

        return $retval;
    }
}

if (!function_exists('boolToInt')) {
    function boolToInt($val) {
        $retval = false;
        if ($val === 1) {
            $retval = true;
        }
        return $retval;
    }
}

if (!function_exists('boolToStr')) {
    function boolToStr($val) {
        switch ($val) {
            case true:
                $retval = 'true';
                break;
            default:
                $retval = 'false';
                break;
        }

        return $retval;
    }
}
