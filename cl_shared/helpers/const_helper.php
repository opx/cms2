<?php

define('DB_PREFIX', 'cl_');
define('VT_OBJECT', 0);
define('VT_INTEGER', 1);
define('VT_FLOAT', 2);
define('VT_STRING', 3);
define('VT_BOOL', 4);

define('SAVE_NEW', 0);
define('SAVE_UPDATE', 1);

if (!function_exists('asset_url')) {
    function asset_url($path, $type = 'public') {
        switch (strtolower($type)) {
            case 'local':
                return base_url($path).'themes/'.$path;
            default:
                return DOMAIN_URL.'assets/'.$path;
        }
    }
}

if (!function_exists('pfxTable')) {
    function pfxTable($table) {
        return DB_PREFIX.$table;
    }
}

if (!function_exists('domainUrl')) {
    function domainUrl($path = '') {
        return DOMAIN_URL.$path;
    }
}
