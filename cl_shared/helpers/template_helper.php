<?php

if (!function_exists('isActiveMenu')) {
    function isActiveMenu($id, $sender='') {
        if (isset($sender)) {
            if ($sender === $id) {
                print 'active js-sidebar-menu';
            }
        } else {
            print '';
        }
    }
}