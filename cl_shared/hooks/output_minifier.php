<?php

function doMinify()
{
    $ci = & get_instance();
    $buffer = $ci->output->get_output();
    $ci->load->shared_helper('minify');
    $buffer = $ci->output->get_output();
    $buffer = minify_html($buffer);

    $ci->output->set_output($buffer);
    $ci->output->_display();
}
