<?php

class MD_DocAttributes extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }

    public function listAttr_Robots() {
        $const = [
            'none'      => 'No Follow, No Index',
            'nofollow'  => 'No Follow',
            'noindex'   => 'No Index',
        ];
        $values = [];
        foreach ($const as $key => $value) {
            $values[] = [
                'id'   => $key,
                'text' => $value,
            ];
        }

        return $values;
    }

    public function listAttr_PostStatuses() {
        $const = [
            'draft'      => 'Draft',
            'published'  => 'Published',
            'hidden'     => 'Hidden',
        ];
        $values = [];
        foreach ($const as $key => $value) {
            $values[] = [
                'id'   => $key,
                'text' => $value,
            ];
        }

        return $values;
    }
}