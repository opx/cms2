<?php

class MD_Languages extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }

    public function listItemsBySlug() {
        $sql =  ' SELECT    slug, language_name, is_default '.
                ' FROM      '.pfxTable('languages');
                ' ORDER BY  language_name ASC ';
        $rows = $this->db->query($sql)->result();
        $retval = [];
        if (!empty($rows)) {
            foreach ($rows as $index => $row) {
                $retval[] = [
                    'id'     => $row->slug,
                    'text'   => $row->language_name,
                    'default'=> $row->is_default,
                ];
            }
        }

        return $retval;
    }

    public function getDefaultBySlug() {
        $sql =  ' SELECT     slug '.
                ' FROM      '.pfxTable('languages').
                ' WHERE      is_default = '.dquoteStr('1').
                ' LIMIT 1 ';
        $obj =  $this->db->query($sql)->row();
        if (!empty($obj)) {
            return $obj->slug;
        } else {
            return 'en';
        }
    }
}