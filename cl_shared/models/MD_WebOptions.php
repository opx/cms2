<?php

class MD_WebOptions extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }

    public function getValue($keyname, $default = '') {
        $sql =  ' SELECT    keyvalue AS value '.
                ' FROM      '.pfxTable('settings').
                ' WHERE     keyname ='.dquoteStr($keyname).
                ' LIMIT 1 ';
        $obj = $this->db->query($sql)->row();
        if (!empty($obj)) {
            return $obj->value;
        } else {
            return $default;
        }
    }
}