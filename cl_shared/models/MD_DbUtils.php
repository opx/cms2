<?php

class MD_DbUtils extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }

    public function isInTable($table, $field, array $value) {
        $fmt_sql = ' SELECT COUNT(%s) AS rcount FROM %s WHERE %s IN (%s)';
        $retval = false;

        $strvalue = '';
        foreach ($value as $index => $val) {
            if (!empty(trim($val))) {
                $strvalue = sprintf('"%s",', $val);
            }
        }
        if (!empty($strvalue)) {
            $strvalue = substr($strvalue, 0, strlen($strvalue)-1);
            $sql = sprintf($fmt_sql, $field, $table, $field, $strvalue);

            $obj = $this->db->query($sql)->row();
            if (!empty($obj)) {
                $retval = (int)$obj->rcount > 0;
            }
        }

        return $retval;
    }

    public function qstrSetCriteriaValue($field, $value, $match_type = 0) {
        if (empty($field)) {
            return '';
        }

        switch ($match_type) {
            case 1:
                $sql = ' LIKE '. dquoteStr('%'.$value);
                break;
            case 2:
                $sql = ' LIKE '. dquoteStr($value.'%');
                break;
            case 3:
                $sql = ' LIKE '. dquoteStr('%'.$value.'%');
                break;
            default:
                $sql = ' = '. dquoteStr($value);
                break;
        }
        return $field.$sql;
    }

    public function qstrSetCriterias(array $fields, $match_type, $operator = 'AND') {
        $sql = '';
        if (empty($fields)) {
            return $sql;
        }

        if (!empty($fields)) {
            $i = 1;
            $count = count($fields);
            foreach ($fields as $key => $value) {
                if ($i < $count) {
                    $sql .= $this->qstrSetCriteriaValue($key, $value, $match_type).' '.$operator.' ';
                } else {
                    $sql .= $this->qstrSetCriteriaValue($key, $value, $match_type);
                }
                $i++;
            }
        }
        return $sql;
    }
}
