# HMVC CI
CI Version : 3.1.4, PHP Version: 7.1


#### - Shared Model
Model bisa digunakan oleh beberapa WebApp

Lokasi: cl_shared/models/
```sh
$this->load->shared_model('Mymodel');
```


#### - Shared Helper
Helper bisa digunakan oleh beberapa WebApp

Lokasi: cl_shared/helpers/
```sh
$this->load->shared_helper('myhelper');
```

#### - Autoload Shared Model & Helper
Buat file baru shared_autoload.php

Lokasi: public/cl_app/config or public/appname/config
```sh
#filename: shared_autoload.php

$autoload['model'] = [];
$autoload['helper'] = [];
``` 

#### - Setting index.php Untuk Setiap WebApp
```sh
#filename: index.php

#ex-1
#main website
$application_folder = 'cl_app'; # sesuaikan nama path
require_once '../env.php'; # sesuaikan dengan relative path

#ex-2:
#2nd app, location: public/blog/
$application_folder = 'cl_app'; # sesuaikan nama path
require_once '../../env.php'; # sesuaikan dengan relative path
``` 

#### Extending Core MY_Loader
Di setiap core WebApp, buat file MY_Loader.php'

Lokasi: public/appname/core/
```sh
#filename: MY_Loader.php

require_once EXTCORE_PATH.'MY_Loader.php';
``` 
