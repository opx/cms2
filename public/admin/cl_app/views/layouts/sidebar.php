<!-- Sidebar -->
<nav id="sidebar">
    <!-- Sidebar Scroll Container -->
    <div id="sidebar-scroll">
        <!-- Sidebar Content -->
        <!-- Adding .sidebar-mini-hide to an element will hide it when the sidebar is in mini mode -->
        <div class="sidebar-content">
            <!-- Side Header -->
            <div class="side-header side-content bg-white-op">
                <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
                <button class="btn btn-link text-gray pull-right hidden-md hidden-lg" type="button" data-toggle="layout" data-action="sidebar_close">
                    <i class="fa fa-times"></i>
                </button>
                <a class="h5 text-white" href="<?php echo base_url(); ?>">
                    <span class="h4 font-w600 sidebar-mini-hide">
                        <?php echo $this->WebOpt->getValue('website_name', 'CMS2'); ?>
                    </span>
                </a>
            </div>
            <!-- END Side Header -->

            <!-- Side Content -->
            <div class="side-content">
                <ul class="nav-main">
                    <li>
                        <a class="<?php isActiveMenu('dashboard', $doc_id); ?>" href="<?php echo base_url('dashboard'); ?>">
                            <i class="si si-speedometer"></i>
                            <span class="sidebar-mini-hide">
                                Dashboard
                            </span>
                        </a>
                    </li>
                    <li>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="javascript:void(0);">
                            <i class="si si-directions"></i>
                            <span class="sidebar-mini-hide">
                                Navigations
                            </span>
                        </a>
                        <ul>
                            <li>
                                <a class="<?php isActiveMenu('nav-menus', $doc_id); ?>" href="<?php echo base_url('navigations/menus'); ?>">
                                    Menus
                                </a>
                            </li>
                            <li>
                                <a class="<?php isActiveMenu('nav-menu_items', $doc_id); ?>" href="<?php echo base_url('navigations/menu-items'); ?>">
                                    Menu Items
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="javascript:void(0);">
                            <i class="si si-layers"></i>
                            <span class="sidebar-mini-hide">
                                Pages
                            </span>
                        </a>
                        <ul>
                            <li>
                                <a class="<?php isActiveMenu('pages', $doc_id); ?>" href="<?php echo base_url('pages'); ?>">
                                    All Pages
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-film"></i>
                            <span class="sidebar-mini-hide">Blogs</span>
                        </a>
                        <ul>
                            <li>
                                <a href="base_tables_styles.html">Categories</a>
                            </li>
                            <li>
                                <a href="base_tables_responsive.html">Blog List</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-note"></i>
                            <span class="sidebar-mini-hide">Portofolios</span>
                        </a>
                        <ul>
                            <li>
                                <a href="base_forms_premade.html">Categories</a>
                            </li>
                            <li>
                                <a href="base_forms_elements.html">All</a>
                            </li>
                            <li>
                                <a href="base_forms_pickers_more.html">Add New</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-bag"></i>
                            <span class="sidebar-mini-hide">Catalogues</span>
                        </a>
                        <ul>
                            <li>
                                <a class="nav-submenu" data-toggle="nav-submenu" href="javascript:;">Categories</a>
                                <ul>
                                    <li>
                                        <a href="base_comp_maps.html">All</a>
                                    </li>
                                    <li>
                                        <a href="base_comp_maps_full.html">Add New</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="base_comp_charts.html">All</a>
                            </li>
                            <li>
                                <a href="base_comp_chartjs_v2.html">Add New</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="si si-puzzle"></i>
                            <span class="sidebar-mini-hide">Galleries</span>
                        </a>
                    </li>
                    <li>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-grid"></i>
                            <span class="sidebar-mini-hide">Blocks</span>
                        </a>
                        <ul>
                            <li>
                                <a href="base_layouts_api.html">All</a>
                            </li>
                            <li>
                                <a href="base_layouts_default.html">Add New</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#">
                            <i class="si si-users"></i><span class="sidebar-mini-hide">Users</span>
                        </a>
                        <ul>
                            <li>
                                <a href="base_layouts_api.html">All</a>
                            </li>
                            <li>
                                <a href="base_layouts_default.html">Add New</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-wrench"></i>
                            <span class="sidebar-mini-hide">Settings</span>
                        </a>
                        <ul>
                            <li>
                                <a href="base_pages_blank.html">Blank</a>
                            </li>
                            <li>
                                <a href="base_pages_dashboard_v2.html">Dashboard v2</a>
                            </li>
                            <li>
                                <a href="base_pages_search.html">Search Results</a>
                            </li>
                            <li>
                                <a href="base_pages_invoice.html">Invoice</a>
                            </li>
                            <li>
                                <a href="base_pages_faq.html">FAQ</a>
                            </li>
                            <li>
                                <a href="base_pages_inbox.html">Inbox</a>
                            </li>
                            <li>
                                <a href="base_pages_files.html">Files</a>
                            </li>
                            <li>
                                <a href="base_pages_tickets.html">Tickets</a>
                            </li>
                            <li>
                                <a href="base_pages_contacts.html">Contacts</a>
                            </li>
                            <li>
                                <a href="base_pages_coming_soon.html">Coming Soon</a>
                            </li>
                            <li>
                                <a href="base_pages_coming_soon_v2.html">Coming Soon v2</a>
                            </li>
                            <li>
                                <a href="base_pages_maintenance.html">Maintenance</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- END Side Content -->
        </div>
        <!-- Sidebar Content -->
    </div>
    <!-- END Sidebar Scroll Container -->
</nav>
<!-- END Sidebar -->