<!DOCTYPE html>

<html class="no-focus" lang="en">
    <head>
        <meta charset="utf-8">

        <title>Admin | <?php echo $doc_title; ?></title>

        <meta name="description" content="CMS2 Framework">
        <meta name="author" content="colour-labs">
        <meta name="robots" content="noindex, nofollow">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="<?php echo asset_url('favicons/favicon.png'); ?>">
        <!-- END Icons -->

        <?php
            addAssetJs('plugins/jquery/jquery.min.js');

            #js plugins
            addAssetCss('plugins/slick/slick.min.css');
            addAssetCss('plugins/slick/slick-theme.min.css');
            addAssetCss('fonts/ui.css');
            addAssetCss('plugins/datatables/jquery.dataTables.min.css');
            addAssetCss('plugins/bootstrap/css/bootstrap.min.css');
            addAssetCss('plugins//jquery-tags-input/jquery.tagsinput.min.css');
            addAssetCss('plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css');
            addAssetCss('plugins/summernote/summernote.min.css');
            addAssetCss('admin/css/core.css', ['id' => 'css-main']);

            addAssetJs('plugins/bootstrap/bootstrap.min.js');
            addAssetJs('plugins/jquery/jquery.slimscroll.min.js');
            addAssetJs('plugins/jquery/jquery.scrollLock.min.js');
            addAssetJs('plugins/jquery/jquery.appear.min.js');
            addAssetJs('plugins/jquery/jquery.countTo.min.js');
            addAssetJs('plugins/jquery/jquery.placeholder.min.js');
            addAssetJs('plugins/js.cookie.min.js');
            addAssetJs('admin/js/core.js');
            addAssetJs('admin/js/common.js');

            addAssetJs('plugins/bootstrap-notify/bootstrap-notify.min.js');
            addAssetJs('plugins/datatables/jquery.dataTables.js');
            addAssetJs('plugins/jquery-tags-input/jquery.tagsinput.min.js');
            addAssetJs('plugins/bootstrap-datetimepicker/moment.min.js');
            addAssetJs('plugins/summernote/summernote.min.js');
            addAssetJs('plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js');
            addAssetJs('plugins/slick/slick.min.js');
            addAssetJs('plugins/datatables/table-render.js');
        ?>
    </head>
    <body>
        <div id="page-loader"></div>

        <!-- Page Container -->
        <div id="page-container" class="sidebar-l sidebar-o side-scroll header-navbar-fixed">
            <?php
            include_once('sidebar.php');
            include_once('navbar.php');
            ?>

            <!-- Main Container -->
            <main id="main-container">
                <?php
                    include_once('header.php');
                    echo isset($doc_content) ? $doc_content : '';
                ?>
            </main>
            <!-- END Main Container -->

            <?php include_once('footer.php'); ?>
        </div>
        <!-- END Page Container -->

        <script>

            jQuery(function(){
                App.initHelpers('slick', 'notify');

                $('#btnVisitMain').on('click', function() {
                    window.open("<?php echo DOMAIN_URL; ?>", '_blank');
                });
            });

        </script>
    </body>
</html>