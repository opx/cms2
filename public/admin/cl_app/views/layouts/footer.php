<!-- Footer -->
<footer id="page-footer" class="content-mini content-mini-full font-s12 bg-gray-lighter clearfix">
    
    <div class="pull-left">
        <a class="font-w600" href="httpS://colour-labs.com" target="_blank">CMS2</a> &copy; <span class="js-year-copy">2017</span>
    </div>
</footer>
<!-- END Footer -->