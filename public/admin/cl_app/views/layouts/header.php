<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div class="col-sm-8">
            <h1 class="page-heading">
                <?php echo $doc_title; ?>
            </h1>
        </div>
        <div class="col-sm-4 text-right hidden-xs">
            <ol class="breadcrumb push-10-t">
                <?php
                    foreach ($doc_breadcrumbs as $index => $item) {
                        echo $item;
                    }
                ?>
            </ol>
        </div>
    </div>
</div>
