jQuery(function () {
    // Init page helpers (Slick Slider plugin)
    App.initHelpers('slick');

    $('#btnVisitMain').on('click', function() {
        window.open("<?php echo BASEDOMAIN_URL; ?>", '_blank');
    });
});