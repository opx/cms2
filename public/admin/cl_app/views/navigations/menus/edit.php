<form class="form-horizontal" id="frmData">
    <?php
    if ($savemode === SAVE_UPDATE) {
        print '<input type="hidden" name="id" value="'.$data->id.'">';
    }
    ?>
    <div class="content">
        <div class="content-grid">
            <div class="row">
                <div class="block">
                    <ul class="nav nav-tabs " data-toggle="tabs">
                        <li class="active">
                            <a href="#tabGeneral">General</a>
                        </li>
                        <li class="pull-right">
                            <div class="block-options-simple btn-group btn-group-sm push-5-t push-15-r">
                                <button class="btn btn-sm btn-primary js-btn-save" type="button" data-toggle="tooltip" title="Submit Page"
                                        data-original-title="Save" style="width:120px">
                                    SUBMIT
                                </button>
                            </div>
                        </li>
                    </ul>
                    <div class="block-content tab-content">
                        <div class="tab-pane active" id="tabGeneral">
                            <div class="form-group">
                                <label class="col-md-2 text-right control-label">
                                    Name
                                    <span class="text-danger push-5-l">*</span>
                                </label>
                                <div class="col-md-10">
                                    <input value="<?php echo isset($data->name) ? $data->name : ''; ?>"
                                            autocomplete="off" maxlength="30"
                                           type="text" name="name" class="form-control js-primarykey"
                                           placeholder="Menu Name">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<script>

$('.js-btn-save').click(function() {
    $.ajax({
        url: "<?php echo '/admin/'.$router['dir'].$router['class'].'/save' ?>",
        type: "POST",
        data: $('#frmData').serialize(),
        dataType: "JSON",
        beforeSend: function() {

        },
        success: function(data) {
            if (!data.status) {
                notifyError('Check your input');
                var $comp;
                for (var key in data.msg) {
                    $comp = $('[name="'+key+'"]');
                    switch(key) {
                        default:
                            $comp.parent().addClass('has-error');
                        break;
                    }
                }
            } else {
                window.location = "<?php echo '/admin/'.$router['dir'].$router['class'] ?>";
            }
        },
        error: function() {

        }
    });
});

</script>