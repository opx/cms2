<div class="content bg-white">
    <div class="block block-opt-refresh-icon7" id="blockTable">
        <div class="block-header">
            <div class="block-options-simple btn-group btn-group-sm">
                <button class="btn btn-primary js-btn-new" type="button" data-toggle="tooltip" title="" data-original-title="Add New Page">
                    Add New
                </button>
                <button class="btn btn-default js-btn-refresh" type="button" data-toggle="tooltip" title="" data-original-title="Refresh">
                    Refresh
                </button>

            </div>
        </div>
        <div class="block-content">
            <div>
                <table class="table table-bordered table-striped display table-vcenter table-condensed table-hover js-datatable" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th width="10%" class="text-center">ID</th>
                            <th width="60%" class="text-left">TITLE</th>
                            <th width="22%" class="text-left">MENU</th>
                            <th width="8%" class="text-center"><i class="fa fa-edit"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    function addActionButtons(id, viewUrl) {
        var html;
        html =  '<div class="btn-group">' +
                '<button class="btn btn-xs btn-primary js-btn-edit" type="button" data-id="'+id+'" title="Edit"><i class="fa fa-pencil"></i></button>' +
                '<button class="btn btn-xs btn-danger js-btn-delete" type="button" data-id="'+id+'" title="Delete"><i class="fa fa-times"></i></button> '+
                '</div>';

        return html;
    }

    function dataListRefresh(f) {
        $('.table').DataTable().ajax.reload();
        if (typeof(f) === 'function') {
            f();
        }
    }

    $(document).ready(function() {
        $('.table').DataTable({
            columnDefs: [
                {
                    orderable: false, targets: [3],
                },
            ],
            responsive: {
                details: false,
            },
            order: [[1, 'asc']],
            bInfo: true,
            serverSide: true,
            iDisplayLength: 10,
            autoWidth: false,
            ajax: {
                url: "menu-items/ajax-datalist",
                data: function(data){
                    data.lang = $('[name="lang_id"]').val();
                },
                beforeSend: function() {
                    App.blocks('#blockTable', 'state_loading');
                },
                complete: function() {
                    App.blocks('#blockTable', 'state_normal');
                },
                error: function() {
                    notifyError('Error while retrieving data');
                }
            },
            processing: false,
            bFilter: true,
            createdRow: function (row, data, index) {
                $('td', row).eq(0).addClass('text-center');
                if (data[3] != 0) {
                    $('td', row).eq(1).addClass('text-italic');
                }
                $('td', row).eq(3).addClass('text-center').html(addActionButtons(data[5]));
            },
            preDrawCallback: function() {

            },
            dom: "<'row'<'col-sm-4'l><'col-sm-4 js-filter-lang'><'col-sm-4'f>>" +
                 "<'row'<'col-sm-12'tr>>" +
                 "<'row'<'col-sm-6'i><'col-sm-6'p>>",
        });

        $('.js-filter-lang').html(
            '<div class="dataTables_length">'+
            '<label><span class="push-5-r">Languge:</span>'+
           '<select class="form-control" name="lang_id">' +
           <?php
            foreach ($languages as $index => $lang) {
                printf('\'<option value="%s"%s>%s</option>\' +', $lang['id'],
                    ($lang['default'] == '1') ? ' selected' : '', $lang['text']
                );
            }
            ?>
           '</select></label></div>'
        ).change(function() {
            dataListRefresh();
        });
    });

    $(document).on('click', '.js-btn-refresh', function() {
        dataListRefresh();
    })

    $(document).on('click', '.js-btn-edit', function() {
        var id = $(this).attr('data-id');
        window.location = 'menus/edit/' + id;
    });

    $(document).on('click', '.js-btn-visitpage', function() {
        window.open($(this).attr('data-url'), '_blank');
    });

    $(document).on('click', '.js-btn-delete', function() {
        if (confirm('Delete Page ? ')) {
            var id = $(this).attr('data-id');
            var status = false;
            $.ajax({
                url: "<?php echo 'menus/delete' ?>",
                dataType: "JSON",
                type: "POST",
                data: {
                    id: id,
                },
                beforeSend: function() {
                    App.blocks('#blockTable', 'state_loading');
                },
                success: function(data) {
                    status = data.status;
                    if (!status) {
                        notifyError(data.msg);
                    } else {
                        dataListRefresh(function() {
                            notifySuccess('Menu has been deleted');
                        });
                    }
                },
                complete: function() {
                    App.blocks('#blockTable', 'state_normal');
                },
                error: function() {
                    notifyError('Internal Server Error');
                },
            });
        }
    });

    $('.js-btn-new').on('click', function() {
        window.location = "<?php echo 'menus/add'; ?>";
    });
</script>
