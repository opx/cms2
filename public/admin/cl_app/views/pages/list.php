<div class="content bg-white">
    <div class="block block-opt-refresh-icon7" id="blockTable">
        <div class="block-header">
            <div class="block-options-simple btn-group btn-group-sm">
                <button class="btn btn-primary js-btn-new" type="button" data-toggle="tooltip" title="" data-original-title="Add New Page">
                    <span class="hidden-xs">Add New</span>
                </button>
                <button class="btn btn-default js-btn-refresh" type="button" data-toggle="tooltip" title="" data-original-title="Refresh">
                    <span class="hidden-xs">Refresh</span>
                </button>

            </div>
        </div>
        <div class="block-content">
            <div>
                <table class="table table-bordered table-striped display table-vcenter table-condensed table-hover js-datatable" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th width="5%" class="text-center">ID</th>
                            <th width="8%" class="text-center">Date</th>
                            <th width="20%" class="text-left">Title</th>
                            <th width="30%" class="text-left">Description</th>
                            <th width="8%" class="text-center">Status</th>
                            <th width="8%" class="text-center"><i class="fa fa-edit"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    function addActionButtons(id, viewUrl) {
        var html;
        html =  '<div class="btn-group">' +
                '<button class="btn btn-xs btn-primary js-btn-edit" type="button" data-id="'+id+'" title="Edit"><i class="fa fa-pencil"></i></button>' +
                '<button class="btn btn-xs btn-danger js-btn-delete" type="button" data-id="'+id+'" title="Delete"><i class="fa fa-times"></i></button> '+
                '<button class="btn btn-xs btn-info js-btn-visitpage" type="button" data-url="'+viewUrl+'" title="View Page"><i class="fa fa-external-link"></i></button> '+
                '</div>';

        return html;
    }

    function dataListRefresh(f) {
        $('.table').DataTable().ajax.reload();
        if (typeof(f) === 'function') {
            f();
        }
    }

    $(document).ready(function() {
        $('.table').DataTable({
            columnDefs: [
                {
                    orderable: false, targets: [5],
                },
            ],
            responsive: {
                details: false,
            },
            order: [[1, 'asc']],
            bInfo: true,
            serverSide: true,
            iDisplayLength: 10,
            autoWidth: false,
            ajax: {
                url: "pages/ajax-datalist",
                beforeSend: function() {
                    App.blocks('#blockTable', 'state_loading');
                },
                complete: function() {
                    App.blocks('#blockTable', 'state_normal');
                },
                error: function() {
                    showNotify('Error retrieving data..', 'danger', ['top', 'center'], 2000, false,
                        function() {}
                    );
                }
            },
            processing: false,
            bFilter: true,
            createdRow: function (row, data, index) {
                $('td', row).eq(0).addClass('text-center');
                $('td', row).eq(1).addClass('text-center');

                var sStatus = data[4].toUpperCase(),
                    sClass = 'label default';
                switch (sStatus) {
                    case 'PUBLISHED':
                        sClass = 'label-success';
                    break;
                    case 'DRAFT':
                        sClass = 'label-danger';
                    break;
                    case 'HIDDEN':
                        sClass = 'label-default';
                    break;
                }
                $('td', row).eq(4).html('<span class="label '+sClass+'">'+sStatus+'</span>').addClass('text-center');
                $('td', row).eq(5).addClass('text-center').html(addActionButtons(data[0], data[5]));
            }
        });

    });

    $(document).on('click', '.js-btn-refresh', function() {
        dataListRefresh();
    })

    $(document).on('click', '.js-btn-edit', function() {
        var id = $(this).attr('data-id');
        window.location = 'pages/edit/' + id;
    });

    $(document).on('click', '.js-btn-visitpage', function() {
        window.open($(this).attr('data-url'), '_blank');
    });

    $(document).on('click', '.js-btn-delete', function() {
        if (confirm('Delete Page ? ')) {
            var id = $(this).attr('data-id');
            var status = false;
            $.ajax({
                url: "<?php echo 'save/delete' ?>",
                dataType: "JSON",
                type: "POST",
                data: {
                    id: id,
                },
                beforeSend: function() {
                    App.blocks('#blockTable', 'state_loading');
                },
                success: function(data) {
                    status = data.status;
                    if (!status) {
                        showNotify(data.msg, 'warning', ['top', 'center'], 1000, false);
                    } else {
                        dataListRefresh(function() {
                            showNotify('Data telah dihapus', 'success', ['top', 'center'], 1000, false);
                        });
                    }
                },
                complete: function() {
                    App.blocks('#blockTable', 'state_normal');
                },
                error: function() {
                    showNotify('Internal Server Error', 'danger', ['top', 'center'], 1000, false);
                },
            });
        }
    });

    $('.js-btn-new').on('click', function() {
        window.location = "<?php echo 'pages/add'; ?>";
    });
</script>
