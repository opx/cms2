<form class="form-horizontal" id="frmData">
    <div class="content">
        <div class="content-grid">
            <div class="row">
                <div class="block">
                    <ul class="nav nav-tabs " data-toggle="tabs">
                        <li class="active">
                            <a href="#tabContent">Content</a>
                        </li>
                        <li>
                            <a href="#tabAttributes">Attributes</a>
                        </li>
                        <li>
                            <a href="#tabSEO">SEO</a>
                        </li>
                        <li class="pull-right">
                            <div class="block-options-simple btn-group btn-group-sm push-5-t push-15-r">
                                <button class="btn btn-sm btn-primary js-btn-save" type="button" data-toggle="tooltip" title="Submit Page"
                                        data-original-title="Save" style="width:120px">
                                    SUBMIT
                                </button>
                            </div>
                        </li>
                    </ul>
                    <div class="block-content tab-content">
                        <div class="tab-pane active" id="tabContent">
                            <div class="form-group">
                                <label class="col-md-2 text-right control-label">Title</label>
                                <div class="col-md-10">
                                    <input value="<?php echo isset($data->page_name) ? $data->page_name : ''; ?>" type="text" name="title" class="form-control" placeholder="Page Title">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <textarea class="js-summernote form-control">
                                        <?php echo isset($data->page_content) ? $data->page_content : ''; ?>
                                    </textarea>
                                </div>

                            </div>
                        </div>

                        <div class="tab-pane" id="tabAttributes">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-2 text-right control-label">Url</label>
                                    <div class="col-md-10">
                                        <div class="input-group">
                                            <span class="input-group-addon"><?php echo domainUrl(); ?></span>
                                            <input value="<?php echo isset($data->page_url) ? $data->page_url : ''; ?>"
                                                   type="text" name="url" class="form-control">
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 text-right control-label" name="status">Status</label>
                                    <div class="col-md-10">
                                        <select class="form-control">
                                            <?php
                                            foreach($doc_statuses as $item) {
                                                $selected = '';
                                                if ($item['id'] === $data->page_status) {
                                                    $selected = ' selected';
                                                }
                                                printf('<option value="%s"%s>%s</option>', $item['id'], $selected, $item['text']);
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 text-right control-label" name="status">Date</label>
                                    <div class="col-md-10">
                                        <div class="js-datetimepicker input-group date">
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                            <input value="<?php echo isset($data->date_added) ? $data->date_added : '' ; ?>" 
                                                   class="form-control" type="text" name="date" placeholder="Select Date..">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 text-right control-label" name="template">Template</label>
                                    <div class="col-md-10">
                                        <select class="form-control">
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 text-right control-label">Featured Image</label>
                                    <div class="col-md-10">

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="tabSEO">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-2 text-right control-label">Keywords</label>
                                    <div class="col-md-10">
                                        <input value="<?php echo isset($data->page_keyword) ? $data->page_keyword : ''; ?>" type="text" name="keyword"
                                        class="js-tags-input form-control" placeholder="Page Keyword" id="example-tags2">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 text-right control-label">Description</label>
                                    <div class="col-md-10">
                                        <textarea class="form-control" placeholder="Description" name="description">
                                            <?php echo isset($data->page_desc) ? $data->page_desc : '' ?>
                                        </textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 text-right control-label">Robots</label>
                                    <div class="col-md-10">
                                        <select class="form-control" name="robot">
                                            <?php
                                            foreach($doc_attributes as $item) {
                                                $selected = '';
                                                if ($item['id'] === $data->page_robots) {
                                                    $selected = ' selected';
                                                }
                                                printf('<option value="%s"%s>%s</option>', $item['id'], $selected, $item['text']);
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

<script type="text/javascript">
    $(document).ready(function() {
        App.initHelpers(['summernote', 'tags-inputs', 'datetimepicker']);

        var tabHeight = $('#tabContent').outerHeight();
        $('.tab-pane').attr('style', 'height:382px');
    })
</script>