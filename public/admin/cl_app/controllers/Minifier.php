<?php

class Minifier extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->shared_helper('minify');
    }

    function extract_filename($filename) {
        $retval = $filename;
        $names = explode('/', $filename);
        $retval = $names[count($names)-1];
        return $retval;
    }

    function extract_filepath($filename) {
        $retval = $filename;
        $f = $this->extract_filename($filename);
        return $retval;
    }

    function js() {
        $file = $this->input->get('url');
        $buffer = file_get_contents($file);
        if (trim($buffer) === '') {
            echo 'File empty';
            exit();
        }
        $buffer = minify_js($buffer);
        $newfile = str_replace('.js', '.min.js', $file);

        echo 'MINIFIED! to '.$newfile;
        file_put_contents($newfile, $buffer);
    }

    function css() {
        $file = $this->input->get('url');
        $buffer = file_get_contents($file);
        if (trim($buffer) === '') {
            echo 'File empty';
            exit();
        }
        $buffer = minify_css($buffer);
        $newfile = str_replace('.css', '.min.css', $file);

        echo 'MINIFIED! to '.$newfile;
        file_put_contents($newfile, $buffer);
    }

    function css_all() {
        $files = [
            '../assets/admin/css/core.css',
        ];
        foreach ($files as $file) {
            $buffer = file_get_contents($file);
            if (trim($buffer) === '') {
                echo 'File empty';
            }

            $buffer = minify_css($buffer);
            $newfile = str_replace('.css', '.min.css', $file);

            echo 'MINIFIED! to '.$newfile.PHP_EOL.'<br/>';
            file_put_contents($newfile, $buffer);
        }
    }

    function js_all() {
        $files = [
            '../assets/admin/js/core.js',
        ];
        foreach ($files as $file) {
            $buffer = file_get_contents($file);
            if (trim($buffer) === '') {
                echo 'File empty';
            }

            $buffer = minify_js($buffer);
            $newfile = str_replace('.js', '.min.js', $file);

            echo 'MINIFIED! to '.$newfile.PHP_EOL.'<br/>';
            file_put_contents($newfile, $buffer);
        }
    }
}
