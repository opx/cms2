<?php

class Dashboard extends MY_Controller
{
    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->model('MD_Doc');
        $doc = & $this->MD_Doc;

        $doc->id = 'dashboard';
        $doc->title = 'Dashboard';
        $doc->breadcrumbs->add('Dashboard', '');

        $doc->content('dashboard');
    }
}