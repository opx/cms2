<?php

class Pages extends MY_Controller
{
    public function __construct() {
        parent::__construct();
    }

    /** INDEX **/
    public function index() {
        $this->load->model('MD_Doc');
        $doc = & $this->MD_Doc;

        $doc->id = 'pages';
        $doc->title = 'Pages';
        $doc->breadcrumbs->add('Pages', '');

        $doc->content('pages/list');
    }
    public function ajax_datalist() {
        $this->load->model('MD_Pages');
        $params = $this->sanitizeInput('get', $_GET);
        echo json_encode($this->MD_Pages->ajaxDataList($params));
    }

    /** EDIT **/
    public function edit($id) {
        $this->load->model('MD_Pages');
        # check if exists
        $data = $this->MD_Pages->getPage($id);
        if (empty($data)) {
            redirect('404');
        }

        $this->load->model('MD_Doc');
        $doc = & $this->MD_Doc;
        $doc->id = 'pages';
        $doc->title = 'Edit Page';
        $doc->breadcrumbs->add('Pages', 'pages');
        $doc->breadcrumbs->add('Edit');

        $this->load->shared_model('MD_DocAttributes');
        $attributes = $this->MD_DocAttributes->listAttr_Robots();
        $statuses = $this->MD_DocAttributes->listAttr_PostStatuses();

        $params = [
            'data'           => $data,
            'doc_attributes' => $attributes,
            'doc_statuses'   => $statuses,
        ];
        $doc->content('pages/edit', $params);
    }
}