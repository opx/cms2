<?php

class Assets extends CI_Controller
{
    public function js($virtual) {
        switch ($virtual) {
            case 'page-base.js':
                echo $this->load->view('layouts/page_base_script', [], true);
                break;
        }
    }
}