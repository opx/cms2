<?php

class Menu_items extends MY_Controller
{
    public function __construct() {
        parent::__construct();
    }

    /** INDEX **/
    public function index() {
        $this->load->model('MD_Doc');
        $doc = & $this->MD_Doc;

        $doc->id = 'nav-menu_items';
        $doc->title = 'Menu Items';
        $doc->breadcrumbs->setItems([
            'Navigations' => '',
            'Menu Items'  => '',
        ]);

        $this->load->shared_model('MD_Languages');
        $langs = $this->MD_Languages->listItemsBySlug();

        $params = [
            'languages' => $langs,
        ];
        $doc->content('navigations/menu_items/list', $params);
    }
    public function ajax_datalist() {
        $this->load->model('MD_MenuItems');
        $params = $this->sanitizeInput('get', $_GET);
        if (isEmptyVar($params['lang'])) {
            $params['lang'] = $this->LangOpt->getDefaultBySlug();
        }
        echo json_encode($this->MD_MenuItems->ajaxDataList($params));
    }

    /** EDIT **/
    public function edit($id) {

        if (isEmptyVar($id)) {
            redirect('navigations/menus');
        }

        $this->load->model('MD_Menus');
        # check if exists
        $data = $this->MD_Menus->getMenu($id);
        if (empty($data)) {
            redirect('404');
        }

        $this->load->model('MD_Doc');
        $doc = & $this->MD_Doc;
        $doc->id = 'nav-menus';
        $doc->title = 'Edit Menu';
        $doc->breadcrumbs->setItems([
            'Navigations' => '',
            'Menus'       => 'navigations/menus',
            'Edit'        => ''
        ]);

        $params = [
            'data'      => $data,
            'router'    => $this->routeUrl(),
            'savemode'  => SAVE_UPDATE,
        ];
        $doc->content('navigations/menus/edit', $params);
    }

    /** NEW **/
    public function add() {
        $this->load->model('MD_Doc');
        $doc = & $this->MD_Doc;
        $doc->id = 'nav-menus';
        $doc->title = 'Edit Menu';
        $doc->breadcrumbs->setItems([
            'Navigations' => '',
            'Menus'       => 'navigations/menus',
            'New'        => ''
        ]);

        $params = [
            'router'    => $this->routeUrl(),
            'savemode'  => SAVE_NEW,
        ];
        $doc->content('navigations/menus/edit', $params);
    }

    function save() {
        $params = $this->sanitizeInput('post', $_POST);
        $this->load->model('MD_Menus');
        $savemode = SAVE_NEW;
        if (!isEmptyVar($params['id'])) {
            $savemode = SAVE_UPDATE;
        }
        $process = $this->MD_Menus->doSave($params, $savemode);
        echo json_encode($process);
    }
}