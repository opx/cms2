<?php

class Menus extends MY_Controller
{
    public function __construct() {
        parent::__construct();
    }

    /** INDEX **/
    public function index() {
        $this->load->model('MD_Doc');
        $doc = & $this->MD_Doc;

        $doc->id = 'nav-menus';
        $doc->title = 'Menus';
        $doc->breadcrumbs->add('Navigations', '');
        $doc->breadcrumbs->add('Menus', '');

        $doc->content('navigations/menus/list');
    }
    public function ajax_datalist() {
        $this->load->model('MD_Menus');
        $params = $this->sanitizeInput('get', $_GET);
        echo json_encode($this->MD_Menus->ajaxDataList($params));
    }

    /** EDIT **/
    public function edit($id) {

        if (isEmptyVar($id)) {
            redirect('navigations/menus');
        }

        $this->load->model('MD_Menus');
        # check if exists
        $data = $this->MD_Menus->getMenu($id);
        if (empty($data)) {
            redirect('404');
        }

        $this->load->model('MD_Doc');
        $doc = & $this->MD_Doc;
        $doc->id = 'nav-menus';
        $doc->title = 'Edit Menu';
        $doc->breadcrumbs->setItems([
            'Navigations' => '',
            'Menus'       => 'navigations/menus',
            'Edit'        => ''
        ]);

        $params = [
            'data'      => $data,
            'router'    => $this->routeUrl(),
            'savemode'  => SAVE_UPDATE,
        ];
        $doc->content('navigations/menus/edit', $params);
    }

    /** NEW **/
    public function add() {
        $this->load->model('MD_Doc');
        $doc = & $this->MD_Doc;
        $doc->id = 'nav-menus';
        $doc->title = 'Edit Menu';
        $doc->breadcrumbs->setItems([
            'Navigations' => '',
            'Menus'       => 'navigations/menus',
            'New'        => ''
        ]);

        $params = [
            'router'    => $this->routeUrl(),
            'savemode'  => SAVE_NEW,
        ];
        $doc->content('navigations/menus/edit', $params);
    }

    public function save() {
        $params = $this->sanitizeInput('post', $_POST);
        $this->load->model('MD_Menus');
        $savemode = SAVE_NEW;
        if (!isEmptyVar($params['id'])) {
            $savemode = SAVE_UPDATE;
        }
        $process = $this->MD_Menus->doSave($params, $savemode);
        echo json_encode($process);
    }

    public function delete() {
        $params = $this->sanitizeInput('post', $_POST);
        $this->load->model('MD_Menus');
        $process = $this->MD_Menus->doDelete($params);
        echo json_encode($process);
    }
}