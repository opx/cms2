<?php

/*
    DATATABLES UTILITIES
    DataTables Output builder for CodeIgniter
    Author: opx (mockheroicx@yahoo.com)
*/

class DataTables
{
    public $column_order;
    public $column_search;
    public $order;
    public $columns;
    public $current_sql;
    protected $db;

    public function __construct() {
        $this->column_search = [];
        $this->column_order = [];
        $this->order = [];
        $this->current_sql = '';

        $ci = & get_instance();
        $ci->load->database();
        $this->db = $ci->db;
    }

    function sqlAsTable($query, $alias = 'aa') {
        return '('.$query.') '.$alias;
    }

    /*
        textlimit
        ---------
            type: array [fieldname => limit_size]
            ex  : original = "abcdef", [original, 2] will display "ab..."
        static_object
        -------------
            type: string
            as a dummy data for output on table such as action buttons and more
    */

    public function getLastQuery() {
        $sql = $this->current_sql;
        $retval = $sql;
        if (!empty($sql)) {
            $retval = substr($sql, 0, strlen($sql)-8);
        }

        return $retval;
    }

    protected function getQuery($sql, $params) {
        $this->current_sql = '';

        $this->db->select('*')
             ->from($this->sqlAsTable($sql));
        $i = 0;
        foreach ($this->column_search as $item) {
            if($params['search']['value'])  {
               if($i===0) {
                   $this->db->group_start();
                   $this->db->like($item, $params['search']['value']);
                }
                else {
                   $this->db->or_like($item, $params['search']['value']);
                }

                if(count($this->column_search) - 1 == $i) {
                   $this->db->group_end();
                }
            }
            $i++;
        }

        if(isset($params['order'])) {
            $this->db->order_by($this->column_order[$params['order']['0']['column']], $params['order']['0']['dir']);
        } else if(isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }

        $this->current_sql = $this->db->last_query();
    }

    public function getResult($sql, $params, array $textlimit = [], $static_object = '') {

        $total_records = 0;

        # calculate all records
        $this->db->select('COUNT(*) AS rec_count')
                 ->from($this->sqlAsTable($sql));

        $obj = $this->db->get()->row();
        if (!empty($obj)) {
            $total_records = (int)$obj->rec_count;
        }
        unset($obj);

        $this->getQuery($sql, $params);
        if($params['length'] != -1) {
            $this->db->limit($params['length'], $params['start']);
        }
        $obj = $this->db->get()->result_array();

        # generate result
        $has_textlimit = !empty($textlimit);
        $data = [];
        $i = 0;
        if (!empty($obj)) {
            foreach ($obj as $item) {
                $rows = [];
                foreach (array_keys($item) as $index => $keyname) {
                    if ($has_textlimit) {
                        foreach ($textlimit as $limitkey => $limitvalue) {
                            $strdata = '';
                            if (strtolower($keyname) === strtolower($limitkey)) {
                                $strdata = limitStr($item[$limitkey], $limitvalue);
                                break;
                            } else {
                                $strdata = $item[$keyname];
                            }
                        }
                        $rows[] = $strdata;
                    } else {
                        $rows[] = $item[$keyname];
                    }
                }
                $rows[] = $static_object;
                $data[] = $rows;
                $i++;
            }
        }

        $this->getQuery($sql, $params);
        $record_filtered = $this->db->get()->num_rows();

        $retval = [
            'draw'            => $params['draw'],
            'recordsTotal'    => $total_records,
            'recordsFiltered' => $record_filtered,
            'data'            => $data,
        ];

        unset($obj);
        return $retval;
    }
}
