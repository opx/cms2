<?php

$autoload['model'] = [
    'MD_DbUtils'    => 'DbUtils',
    'MD_WebOptions' => 'WebOpt',
    'MD_Languages'  => 'LangOpt',
];

$autoload['helper'] = [
    'minify', 'const', 'htmldoc', 'routines', 'strutils',
];

$autoload['library'] = [];
