<?php

//require_once EXTTHIRDPARTY_PATH.'MX'.DIRECTORY_SEPARATOR.'Loader.php';

class MY_Loader extends CI_Loader
{
    protected $_ci_sharedlib_paths = [SHAREDLIB_PATH];

    public function __construct() {
        parent::__construct();
        $this->_ci_shared_autoloader();
    }

    protected function _ci_shared_autoloader() {
        if (file_exists(APPPATH.'config/shared_autoload.php')) {
			include(APPPATH.'config/shared_autoload.php');
		}

		if (file_exists(APPPATH.'config/'.ENVIRONMENT.'/shared_autoload.php')) {
			include(APPPATH.'config/'.ENVIRONMENT.'/shared_autoload.php');
		}

		if ( ! isset($autoload)) {
			return;
		}

        # helpers
		if (isset($autoload['helper'])) {
            foreach ($autoload['helper'] as $helper) {
                $this->shared_helper($helper);
            }
        }

        # models
        if (isset($autoload['model'])) {
			$this->shared_model($autoload['model']);
	    }
    }

    public function shared_model($model, $name = '', $db_conn = FALSE)
	{
		if (empty($model)) {
			return $this;
		}
		elseif (is_array($model)) {
			foreach ($model as $key => $value) {
				is_int($key) ? $this->shared_model($value, '', $db_conn) : $this->shared_model($key, $value, $db_conn);
			}

			return $this;
		}

		$path = '';

		// Is the model in a sub-folder? If so, parse out the filename and path.
		if (($last_slash = strrpos($model, '/')) !== FALSE)
		{
			// The path is in front of the last slash
			$path = substr($model, 0, ++$last_slash);

			// And the model name behind it
			$model = substr($model, $last_slash);
		}

		if (empty($name))
		{
			$name = $model;
		}

		if (in_array($name, $this->_ci_models, TRUE))
		{
			return $this;
		}

		$CI =& get_instance();
		if (isset($CI->$name))
		{
			throw new RuntimeException('The model name you are loading is the name of a resource that is already being used: '.$name);
		}

		if ($db_conn !== FALSE && ! class_exists('CI_DB', FALSE))
		{
			if ($db_conn === TRUE)
			{
				$db_conn = '';
			}

			$this->database($db_conn, FALSE, TRUE);
		}


		if ( ! class_exists('CI_Model', FALSE))
		{
            //$app_path = APPPATH.'core'.DIRECTORY_SEPARATOR;
            $app_path = SHAREDLIB_PATH;
            if (file_exists($app_path.'Model.php'))
			{
				require_once($app_path.'Model.php');
				if ( ! class_exists('CI_Model', FALSE))
				{
					throw new RuntimeException($app_path."Model.php exists, but doesn't declare class CI_Model");
				}
			}
			elseif ( ! class_exists('CI_Model', FALSE))
			{
				require_once(BASEPATH.'core'.DIRECTORY_SEPARATOR.'Model.php');
			}

			$class = config_item('subclass_prefix').'Model';
			if (file_exists($app_path.$class.'.php'))
			{
				require_once($app_path.$class.'.php');
				if ( ! class_exists($class, FALSE))
				{
					throw new RuntimeException($app_path.$class.".php exists, but doesn't declare class ".$class);
				}
			}
		}

		$model = ucfirst($model);
		if ( ! class_exists($model, FALSE))
		{
			foreach ($this->_ci_sharedlib_paths as $mod_path)
			{
				if ( ! file_exists($mod_path.'models'.DIRECTORY_SEPARATOR.$path.$model.'.php'))
				{
					continue;
				}

				require_once($mod_path.'models/'.$path.$model.'.php');
				if ( ! class_exists($model, FALSE))
				{
					throw new RuntimeException($this->$mod_path."models/".$path.$model.".php exists, but doesn't declare class ".$model);
				}

				break;
			}

			if ( ! class_exists($model, FALSE))
			{
				throw new RuntimeException('Unable to locate the model you have specified: '.$model);
			}
		}
		elseif ( ! is_subclass_of($model, 'CI_Model'))
		{
			throw new RuntimeException("Class ".$model." already exists and doesn't extend CI_Model");
		}

		$this->_ci_models[] = $name;
		$CI->$name = new $model();
		return $this;
	}

    public function shared_helper($helpers = array())
    {
        is_array($helpers) OR $helpers = array($helpers);
        foreach ($helpers as &$helper)
        {
            $filename = basename($helper);
            $filepath = ($filename === $helper) ? '' : substr($helper, 0, strlen($helper) - strlen($filename));
            $filename = strtolower(preg_replace('#(_helper)?(\.php)?$#i', '', $filename)).'_helper';
            $helper   = $filepath.$filename;

            if (isset($this->_ci_helpers[$helper]))
            {
                continue;
            }

            // Is this a helper extension request?
            $ext_helper = config_item('subclass_prefix').$helper;
            $ext_loaded = FALSE;
            foreach ($this->_ci_sharedlib_paths as $path)
            {
                if (file_exists($path.'helpers/'.$ext_helper.'.php'))
                {
                    include_once($path.'helpers/'.$ext_helper.'.php');
                    $ext_loaded = TRUE;
                }
            }

            // If we have loaded extensions - check if the base one is here
            if ($ext_loaded === TRUE)
            {
                $base_helper = SHAREDLIB_PATH.'helpers/'.$helper.'.php';

                if ( ! file_exists($base_helper))
                {
                    show_error('Unable to load the requested file: cl_shared/helpers/'.$helper.'.php');
                }

                include_once($base_helper);
                $this->_ci_helpers[$helper] = TRUE;
                log_message('info', 'Shared Helper loaded: '.$helper);
                continue;
            }

            // No extensions found ... try loading regular helpers and/or overrides
            foreach ($this->_ci_sharedlib_paths as $path)
            {

                if (file_exists($path.'helpers/'.$helper.'.php'))
                {
                    include_once($path.'helpers/'.$helper.'.php');

                    $this->_ci_helpers[$helper] = TRUE;
                    log_message('info', 'Shared Helper loaded: '.$helper);
                    break;
                }
            }

            // unable to load the helper
            if ( ! isset($this->_ci_helpers[$helper]))
            {
                show_error('Unable to load the requested file: cl_shared/helpers/'.$helper.'.php');
            }
        }

        return $this;
    }

    public function view_minified($file, $parameters = [], $as_source = false) {
    	//$buffer = & get_instance()->load->view($file, $parameters, $as_source);

    	//return minify_html($buffer);
    }
}
