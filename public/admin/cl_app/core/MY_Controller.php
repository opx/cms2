<?php

class MY_Controller extends CI_Controller
{
    public function __construct() {
        parent::__construct();
    }

    public function sanitizeInput($method, $input, $escape_html = true) {
        $output = [];
        if (empty($input)) {
            return $output;
        }

        switch (strtoupper($method)) {
            case 'POST':
                foreach ($input as $key => $value) {
                    if ($escape_html) {
                        $output[$key] = html_escape($this->input->post($key));
                    } else {
                        $output[$key] = $this->input->post($key);
                    }
                }
                break;
            case 'GET':
                foreach ($input as $key => $value) {
                    if ($escape_html) {
                        $output[$key] = html_escape($this->input->get($key));
                    } else {
                        $output[$key] = $this->input->get($key);
                    }
                }
                break;
            default:
                break;
        }

        return $output;
    }

    public function routeUrl() {
        return [
            'dir'   => $this->router->fetch_directory(),
            'class' => $this->router->fetch_class(),
        ];
    }
}