<?php

class MD_Doc extends CI_Model
{
    public $id = '';
    public $title = '';
    public $breadcrumbs = null;

    public function __construct() {
        parent::__construct();

        $this->load->shared_helper('template');

        $this->breadcrumbs = new Comp_Breadcrumbs();
        $this->breadcrumbs->homeUrl('AsIcon', 'Home', '');
    }

    function _setOptions() {
        return [
            'doc_id'     => $this->id,
            'doc_title'  => $this->title,
        ];
    }

    public function content($view, array $content_params = []) {
        $init_params = array_merge($this->_setOptions(), $content_params);
        $html = $this->load->view($view, $init_params, true);

        $params = $init_params;
        $params['doc_content'] = $html;
        $params['doc_breadcrumbs'] = $this->breadcrumbs->render();
        $this->load->view('layouts/main', $params);
    }
}

class Comp_Breadcrumbs
{
    protected $_links;
    protected $_home;
    protected $_homeAsIcon;

    public function __construct() {
        $this->_links = [];
        $this->_home = [];
        $this->_homeAsIcon = false;

        get_instance()->load->helper('url');
    }

    public function setItems(array $items) {
        if (empty($items)) {
            return;
        }

        foreach ($items as $text => $url) {
            $this->add($text, $url);
        }
    }

    public function homeUrl($arg, $title, $link) {
        if ($arg === 'AsIcon') {
            $this->_homeAsIcon = true;
        }
        $this->_home = ['title'=>$title, 'url'=>base_url($link)];
    }

    public function add($title, $link = '') {
        if (!empty($link)) {
            $this->_links[] = ['title'=>$title, 'url'=>base_url($link)];
        } else {
            $this->_links[] = ['title'=>$title, 'url'=>''];
        }
    }

    public function render() {
        $newlinks = [];
        $fmt = '<li%s>%s</li>';
        if (!empty($this->_home)) {
            $newlinks[] = $this->_home;
        }
        foreach ($this->_links as $link) {
            $newlinks[] = $link;
        }

        #resturcture html
        $output = [];
        $fmt = '<li%s>%s</li>';
        $fmt_url = '<a href="%s" title="%s">%s</a>';

        $i = 0;
        foreach ($newlinks as $link) {
            if ($i === 0) {
                if (!empty($this->_home)) {
                    if ($this->_homeAsIcon) {
                        $href = sprintf($fmt_url, $link['url'], $link['title'],
                                '<i class="fa fa-home"></i>');
                    } else {
                        $href = sprintf($fmt_url, $link['url'], $link['title'],
                                $link['title']);
                    }
                    $output[] = sprintf($fmt, '', $href);
                }
            } else {
                if ($i+1 === count($newlinks)) {
                    $href = $link['title'];
                    $class = ' class="active"';
                } else {
                    if (!empty($link['url'])) {
                        $href = sprintf($fmt_url, $link['url'], $link['title'],
                                $link['title']);
                        $class = '';
                    } else {
                        $href = $link['title'];
                        $class = ' class="active"';
                    }

                }
                $output[] = sprintf($fmt, ' class="active"', $href);
            }
            $i++;
        }

        return $output;
    }
}