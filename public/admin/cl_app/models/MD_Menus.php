<?php

class MD_Menus extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }

    public function ajaxDataList($params) {
        $sql =  ' SELECT    id, name '.
                ' FROM      '.pfxTable('menus');

        $this->load->library('datatables');
        $this->datatables->column_search = ['id', 'name'];
        $this->datatables->column_order = ['id', 'name',];
        $this->datatables->order = ['id' => 'desc'];

        $textlimit = [
            'title' => 120,
        ];
        $detils = $this->datatables->getResult($sql, $params, $textlimit);

        return $detils;
    }

    public function getMenu($id) {
        $sql =  ' SELECT     id, name '.
                ' FROM      '.pfxTable('menus').
                ' WHERE     id = '.dquoteStr($id).
                ' LIMIT 1 ';
        $obj = $this->db->query($sql)->row();
        return $obj;
    }

    public function doSave($params, $savemode) {
        $process = setValidation(true);

        $this->load->library('form_validation');
        $validator = &$this->form_validation;
        $validator->set_data($params);
        $validator->set_rules('name', 'Name', 'trim|required|min_length[5]', [
            'required'  => '%s required',
        ]);

        if (!$validator->run()) {
            $process = setValidation(false, $validator->error_array());
        }

        if ($process['status']) {
            switch ($savemode) {
                case SAVE_NEW:
                    $this->db->insert(pfxTable('menus'), [
                        'name'  => $params['name'],
                    ]);
                break;
                case SAVE_UPDATE:
                    $this->db->where('id', $params['id'])
                         ->update(pfxTable('menus'), [
                             'name' => $params['name'],
                    ]);
                break;
            }
        }

        return $process;
    }

    public function doDelete($params) {
        $process = setValidation(true);

        $this->load->library('form_validation');
        $validator = &$this->form_validation;
        $validator->set_data($params);
        $validator->set_rules('id', 'ID', 'trim|required', [
            'required'  => '%s empty',
        ]);

        if (!$validator->run()) {
            $process = setValidation(false, $validator->error_array());
        }

        if ($process['status']) {
            $sql =  ' SELECT      aa.id '.
                    ' FROM        cl_menu_item_translations aa '.
                    ' LEFT JOIN   cl_menu_items bb ON aa.item_id = bb.id '.
                    ' LEFT JOIN   cl_menus cc ON bb.menu_id = cc.id ';
            $detils = $this->db->query($sql)->result();

            if (!empty($detils)) {
                foreach ($detils as $row) {
                    $this->db->where('id', $row->id)
                             ->delete(pfxTable('menu_item_translations'));
                }
            }
            $this->db->where('id', $params['id'])
                     ->delete(pfxTable('menus'));
            $this->db->where('menu_id', $params['id'])
                     ->delete(pfxTable('menu_items'));
        }

        return $process;
    }
}