<?php

class MD_Pages extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }

    public function ajaxDataList($params) {
        $sql =  ' SELECT    page_id, date_added, page_name, page_desc, page_status, page_url'.
                ' FROM      '.pfxTable('pages');

        $this->load->library('datatables');
        $this->datatables->column_search = ['page_id', 'date_added', 'page_name', 'page_desc', 'page_status'];
        $this->datatables->column_order = ['page_id', 'date_added', 'page_name', 'page_desc', 'page_status'];
        $this->datatables->order = ['page_id' => 'desc'];

        $textlimit = [
            'page_desc' => 120,
        ];
        $detils = $this->datatables->getResult($sql, $params, $textlimit);

        return $detils;
    }

    public function getPage($page_id) {
        $sql =  ' SELECT    aa.*, bb.first_name, bb.last_name '.
                ' FROM      '.pfxTable('pages aa ').
                ' LEFT JOIN '.pfxTable('users bb '). ' ON aa.page_author = bb.id '.
                ' WHERE     aa.page_id = '.dquoteStr($page_id).
                ' LIMIT 1 ';
        $obj = $this->db->query($sql)->row();

        if (!empty($obj)) {
            return $obj;
        } else {
            return;
        }
    }


}