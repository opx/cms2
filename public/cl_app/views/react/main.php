<html>
<head>
<?php
    addAssetJs('js/react/react.js');
    addAssetJs('js/react/react-dom.js');
    addAssetJs('js/react/react-with-addons.js');
    addAssetJs('js/babel-core/browser.min.js');
?>
</head>
<body>

<div>
    <form>
        <input type="text" name="txt"><button type="submit">Post</button>
    </form>
</div>
<div id="view"></div>
<script type="text/babel">
    

    var Hello = React.createClass({
        render: function() {
            return (
                <div>
                    <button type="button">Tes</button>
                </div>
            );
        }
    });

    var Msg = React.createClass({
        render: function() {
            return(
                <div>
                    {this.props.message}
                </div>
            );
        }
    })

    ReactDOM.render(<Hello/>, document.getElementById('view'));
</script>

</body>
</html>
