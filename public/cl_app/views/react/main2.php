<html>
<head>
    <?php
        addAssetCss('plugins/bootstrap/css/bootstrap.css');

        addAssetJs('plugins/jquery/jquery.min.js');
        addAssetJs('plugins/bootstrap/js/bootstrap.js');
        addAssetJs('plugins/react/react.min.js');
        addAssetJs('plugins/react/react-dom.min.js');
        addAssetJs('plugins/browser.min.js');
    ?>
</head>
<body>

    <script>
        var Tes = React.createClass({
            render: function() {
                return (<span>Tes</span>);            }
        });

        ReactDOM.render(<Tes/>);
    </script>

</body>

</html>