function showNotify(msg, tipe, pos, delaytime, allow_close, closeCallback) {
    return $.notify(
        {
            message: msg,
        },
        {
            type: tipe,
            allow_dismis: allow_close,
            newest_on_top: true,
            placement: {
                from: pos[0],
                align: pos[1],
            },
            delay: delaytime,
            animate: {
		        enter: 'animated fadeInDown',
		        exit: 'animated fadeOutUp',
	        },
            onClosed: function() {
                if (typeof(closeCallback) == 'function') {
                    closeCallback();
                }
            },
        }
    );
}

function showAlert(target,tipe,msg) {
    var html = '<div class="alert ' + tipe + ' fade in">' +
               '<a href="#" class="close" data-dismiss="alert">&times;</a>'+
               msg + '</div>';
    $(target).html(html);
}

function focusNextElement() {
    var focussableElements = 'a:not([disabled]), button:not([disabled]), input[type=text]:not([disabled]), [tabindex]:not([disabled]):not([tabindex="-1"])';
    if (document.activeElement && document.activeElement.form) {
        var focussable = Array.prototype.filter.call(document.activeElement.form.querySelectorAll(focussableElements),
        function(element) {
            return element.offsetWidth > 0 || element.offsetHeight > 0 || element === document.activeElement
        });
        var index = focussable.indexOf(document.activeElement);
        focussable[index + 1].focus();
    }
}