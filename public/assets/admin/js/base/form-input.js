function clearErrorValidation(e) {
    if (e) {
        $(e).parents('.form-group').removeClass('has-error');
        $(e).parents('.form-group > div').children('.error-block').remove();
    } else {
        $('.error-block').remove();
        $('.has-error').removeClass('has-error');
    }
}