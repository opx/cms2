$('.cbx-filter-specify').on('change', function() {
    var idx = $(this).index(),
        enabled = $(this).is(':checked');

    $($('.input-filter-specify')[idx]).attr('disabled', !enabled);
});

$(document).ready(function() {
    $('.table-filter-specify').dataTable ({
        order: [],
        columnDefs: [{
            targets: [0,2],
            orderable: false,
        }],
        bInfo: false,
        serverSide: false,
        processing: false,
        bFilter: false,
        bPaginate: false,
        bAutoWidth: false,
        
        scrollCollapse: true,
    });
});