function showNotify(msg, tipe, pos, icon, delaytime, allow_close, closeCallback) {
    return $.notify(
        {
            message: msg,
            icon: icon,
        },
        {

            type: tipe,
            allow_dismis: allow_close,
            newest_on_top: true,
            placement: {
                from: pos[0],
                align: pos[1],
            },
            delay: delaytime,
            animate: {
		        enter: 'animated fadeInDown',
		        exit: 'animated fadeOutUp',
	        },
            onClosed: function() {
                if (typeof(closeCallback) == 'function') {
                    closeCallback();
                }
            },
        }
    );
}

function notifyError(text, closeCallback) {
    showNotify(text, 'danger', ['top', 'center'], 'fa fa-warning', 1000, true, closeCallback);
}

function notifySuccess(text, closeCallback) {
    showNotify(text, 'success', ['top', 'center'], 'fa fa-check', 1000, true, closeCallback);
}

function notifyWarning(text, closeCallback) {
    showNotify(text, 'warning', ['top', 'center'], 'fa fa-warning', 1000, true, closeCallback);
}

function notifyInfo(text, closeCallback) {
    showNotify(text, 'info', ['top', 'center'], 'fa fa-info-circle', 1000, true, closeCallback);
}

function showAlert(target,tipe,msg) {
    var html = '<div class="alert ' + tipe + ' fade in">' +
               '<a href="#" class="close" data-dismiss="alert">&times;</a>'+
               msg + '</div>';
    $(target).html(html);
}

function focusNextElement() {
    var focussableElements = 'a:not([disabled]), button:not([disabled]), input[type=text]:not([disabled]), [tabindex]:not([disabled]):not([tabindex="-1"])';
    if (document.activeElement && document.activeElement.form) {
        var focussable = Array.prototype.filter.call(document.activeElement.form.querySelectorAll(focussableElements),
        function(element) {
            return element.offsetWidth > 0 || element.offsetHeight > 0 || element === document.activeElement
        });
        var index = focussable.indexOf(document.activeElement);
        focussable[index + 1].focus();
    }
}

jQuery(function(){
    App.initHelpers('slick');

    $('.js-primarykey').keydown(function(e) {
        if (e.keyCode == '32') {
            e.preventDefault();
        }
    });
});
